﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: SamplenamesController
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling samplenames. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class SamplenamesController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public SamplenamesController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Samplenames. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The samplename. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Samplename>>> GetSamplename()
        {
            return await _context.Samplename.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Samplenames/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The samplename. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Samplename>> GetSamplename(decimal id)
        {
            var samplename = await _context.Samplename.FindAsync(id);

            if (samplename == null)
            {
                return NotFound();
            }

            return samplename;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Samplenames/5 To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">           The identifier. </param>
        /// <param name="samplename">   The samplename. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutSamplename(decimal id, Samplename samplename)
        {
            if (id != samplename.Id)
            {
                return BadRequest();
            }

            _context.Entry(samplename).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SamplenameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Samplenames To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateException">    Thrown when a Database Update error condition occurs. </exception>
        ///
        /// <param name="samplename">   The samplename. </param>
        ///
        /// <returns>   An ActionResult&lt;Samplename&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Samplename>> PostSamplename(Samplename samplename)
        {
            _context.Samplename.Add(samplename);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SamplenameExists(samplename.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSamplename", new { id = samplename.Id }, samplename);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Samplenames/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Samplename&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Samplename>> DeleteSamplename(decimal id)
        {
            var samplename = await _context.Samplename.FindAsync(id);
            if (samplename == null)
            {
                return NotFound();
            }

            _context.Samplename.Remove(samplename);
            await _context.SaveChangesAsync();

            return samplename;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given samplename exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool SamplenameExists(decimal id)
        {
            return _context.Samplename.Any(e => e.Id == id);
        }
    }
}
