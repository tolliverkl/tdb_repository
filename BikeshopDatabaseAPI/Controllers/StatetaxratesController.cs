﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: StatetaxratesController
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling statetaxrates. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class StatetaxratesController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public StatetaxratesController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Statetaxrates. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The statetaxrate. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Statetaxrate>>> GetStatetaxrate()
        {
            return await _context.Statetaxrate.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Statetaxrates/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The statetaxrate. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Statetaxrate>> GetStatetaxrate(string id)
        {
            var statetaxrate = await _context.Statetaxrate.FindAsync(id);

            if (statetaxrate == null)
            {
                return NotFound();
            }

            return statetaxrate;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Statetaxrates/5 To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">           The identifier. </param>
        /// <param name="statetaxrate"> The statetaxrate. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutStatetaxrate(string id, Statetaxrate statetaxrate)
        {
            if (id != statetaxrate.State)
            {
                return BadRequest();
            }

            _context.Entry(statetaxrate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StatetaxrateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Statetaxrates To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateException">    Thrown when a Database Update error condition occurs. </exception>
        ///
        /// <param name="statetaxrate"> The statetaxrate. </param>
        ///
        /// <returns>   An ActionResult&lt;Statetaxrate&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Statetaxrate>> PostStatetaxrate(Statetaxrate statetaxrate)
        {
            _context.Statetaxrate.Add(statetaxrate);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (StatetaxrateExists(statetaxrate.State))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetStatetaxrate", new { id = statetaxrate.State }, statetaxrate);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Statetaxrates/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Statetaxrate&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Statetaxrate>> DeleteStatetaxrate(string id)
        {
            var statetaxrate = await _context.Statetaxrate.FindAsync(id);
            if (statetaxrate == null)
            {
                return NotFound();
            }

            _context.Statetaxrate.Remove(statetaxrate);
            await _context.SaveChangesAsync();

            return statetaxrate;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given statetaxrate exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool StatetaxrateExists(string id)
        {
            return _context.Statetaxrate.Any(e => e.State == id);
        }
    }
}
