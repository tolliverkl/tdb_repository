﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: ComponentsController
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling components. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class ComponentsController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public ComponentsController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Components. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The component. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Component>>> GetComponent()
        {
            return await _context.Component.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Components/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The component. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Component>> GetComponent(decimal id)
        {
            var component = await _context.Component.FindAsync(id);

            if (component == null)
            {
                return NotFound();
            }

            return component;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Components/5 To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">           The identifier. </param>
        /// <param name="component">    The component. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutComponent(decimal id, Component component)
        {
            if (id != component.Componentid)
            {
                return BadRequest();
            }

            _context.Entry(component).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ComponentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Components To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="component">    The component. </param>
        ///
        /// <returns>   An ActionResult&lt;Component&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Component>> PostComponent(Component component)
        {
            _context.Component.Add(component);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetComponent", new { id = component.Componentid }, component);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Components/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Component&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Component>> DeleteComponent(decimal id)
        {
            var component = await _context.Component.FindAsync(id);
            if (component == null)
            {
                return NotFound();
            }

            _context.Component.Remove(component);
            await _context.SaveChangesAsync();

            return component;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given component exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool ComponentExists(decimal id)
        {
            return _context.Component.Any(e => e.Componentid == id);
        }
    }
}
