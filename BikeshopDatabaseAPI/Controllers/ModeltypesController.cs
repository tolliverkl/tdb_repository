﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: ModeltypesController
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling modeltypes. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class ModeltypesController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public ModeltypesController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Modeltypes. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The modeltype. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Modeltype>>> GetModeltype()
        {
            return await _context.Modeltype.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Modeltypes/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The modeltype. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Modeltype>> GetModeltype(string id)
        {
            var modeltype = await _context.Modeltype.FindAsync(id);

            if (modeltype == null)
            {
                return NotFound();
            }

            return modeltype;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Modeltypes/5 To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">           The identifier. </param>
        /// <param name="modeltype">    The modeltype. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutModeltype(string id, Modeltype modeltype)
        {
            if (id != modeltype.Modeltype1)
            {
                return BadRequest();
            }

            _context.Entry(modeltype).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModeltypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Modeltypes To protect from overposting attacks, enable the specific properties
        ///     you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateException">    Thrown when a Database Update error condition occurs. </exception>
        ///
        /// <param name="modeltype">    The modeltype. </param>
        ///
        /// <returns>   An ActionResult&lt;Modeltype&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Modeltype>> PostModeltype(Modeltype modeltype)
        {
            _context.Modeltype.Add(modeltype);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ModeltypeExists(modeltype.Modeltype1))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetModeltype", new { id = modeltype.Modeltype1 }, modeltype);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Modeltypes/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Modeltype&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Modeltype>> DeleteModeltype(string id)
        {
            var modeltype = await _context.Modeltype.FindAsync(id);
            if (modeltype == null)
            {
                return NotFound();
            }

            _context.Modeltype.Remove(modeltype);
            await _context.SaveChangesAsync();

            return modeltype;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given modeltype exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool ModeltypeExists(string id)
        {
            return _context.Modeltype.Any(e => e.Modeltype1 == id);
        }
    }
}
