﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: BicycletubeusagesController
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BikeShopDatabaseAPI.Models;

namespace BikeShopDatabaseAPI.Controllers
{
    ///-------------------------------------------------------------------------------------------------
    /// <summary>   A controller for handling bicycletubeusages. </summary>
    ///
    /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
    ///-------------------------------------------------------------------------------------------------

    [Route("api/[controller]")]
    [ApiController]
    public class BicycletubeusagesController : ControllerBase
    {
        /// <summary>   The context. </summary>
        private readonly BikeShopContext _context;

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="context">  The context. </param>
        ///-------------------------------------------------------------------------------------------------

        public BicycletubeusagesController(BikeShopContext context)
        {
            _context = context;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Bicycletubeusages. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <returns>   The bicycletubeusage. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Bicycletubeusage>>> GetBicycletubeusage()
        {
            return await _context.Bicycletubeusage.ToListAsync();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   GET: api/Bicycletubeusages/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   The bicycletubeusage. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpGet("{id}")]
        public async Task<ActionResult<Bicycletubeusage>> GetBicycletubeusage(decimal id)
        {
            var bicycletubeusage = await _context.Bicycletubeusage.FindAsync(id);

            if (bicycletubeusage == null)
            {
                return NotFound();
            }

            return bicycletubeusage;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     PUT: api/Bicycletubeusages/5 To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <exception cref="DbUpdateConcurrencyException"> Thrown when a Database Update Concurrency
        ///                                                 error condition occurs. </exception>
        ///
        /// <param name="id">               The identifier. </param>
        /// <param name="bicycletubeusage"> The bicycletubeusage. </param>
        ///
        /// <returns>   An IActionResult. </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPut("{id}")]
        public async Task<IActionResult> PutBicycletubeusage(decimal id, Bicycletubeusage bicycletubeusage)
        {
            if (id != bicycletubeusage.Serialnumber)
            {
                return BadRequest();
            }

            _context.Entry(bicycletubeusage).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BicycletubeusageExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>
        ///     POST: api/Bicycletubeusages To protect from overposting attacks, enable the specific
        ///     properties you want to bind to, for more details, see
        ///     https://go.microsoft.com/fwlink/?linkid=2123754.
        /// </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="bicycletubeusage"> The bicycletubeusage. </param>
        ///
        /// <returns>   An ActionResult&lt;Bicycletubeusage&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpPost]
        public async Task<ActionResult<Bicycletubeusage>> PostBicycletubeusage(Bicycletubeusage bicycletubeusage)
        {
            _context.Bicycletubeusage.Add(bicycletubeusage);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBicycletubeusage", new { id = bicycletubeusage.Serialnumber }, bicycletubeusage);
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   DELETE: api/Bicycletubeusages/5. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An ActionResult&lt;Bicycletubeusage&gt; </returns>
        ///-------------------------------------------------------------------------------------------------

        [HttpDelete("{id}")]
        public async Task<ActionResult<Bicycletubeusage>> DeleteBicycletubeusage(decimal id)
        {
            var bicycletubeusage = await _context.Bicycletubeusage.FindAsync(id);
            if (bicycletubeusage == null)
            {
                return NotFound();
            }

            _context.Bicycletubeusage.Remove(bicycletubeusage);
            await _context.SaveChangesAsync();

            return bicycletubeusage;
        }

        ///-------------------------------------------------------------------------------------------------
        /// <summary>   Queries if a given bicycletubeusage exists. </summary>
        ///
        /// <remarks>   Table Drop Bikes, 9/18/2020. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        ///-------------------------------------------------------------------------------------------------

        private bool BicycletubeusageExists(decimal id)
        {
            return _context.Bicycletubeusage.Any(e => e.Serialnumber == id);
        }
    }
}
