﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Componentname.cs
 * Summary: Serves as a model for a Componentname
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Componentname
    {
        public Componentname()
        {
            Component = new HashSet<Component>();
        }

        public string Componentname1 { get; set; }
        public decimal? Assemblyorder { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Component> Component { get; set; }
    }
}
