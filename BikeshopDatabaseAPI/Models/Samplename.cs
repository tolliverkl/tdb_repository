﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: samplename
 * Summary: Serves as a model for a samplename 
 */

using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Samplename
    {
        public decimal Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Gender { get; set; }
    }
}
