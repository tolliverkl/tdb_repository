﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Employee.cs
 * Summary: Serves as a model for a Employee
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Employee
    {
        public Employee()
        {
            Bicycle = new HashSet<Bicycle>();
            Bikeparts = new HashSet<Bikeparts>();
            Purchaseorder = new HashSet<Purchaseorder>();
        }

        public decimal Employeeid { get; set; }
        public string Taxpayerid { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Homephone { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public decimal? Cityid { get; set; }
        public DateTime? Datehired { get; set; }
        public DateTime? Datereleased { get; set; }
        public decimal? Currentmanager { get; set; }
        public decimal? Salarygrade { get; set; }
        public decimal? Salary { get; set; }
        public string Title { get; set; }
        public string Workarea { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Bicycle> Bicycle { get; set; }
        public virtual ICollection<Bikeparts> Bikeparts { get; set; }
        public virtual ICollection<Purchaseorder> Purchaseorder { get; set; }
    }
}
