﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: TubeMaterial
 * Summary: Serves as a model for a TubeMaterial
 */
using System;
using System.Collections.Generic;

namespace BikeShopDatabaseAPI.Models
{
    public partial class Tubematerial
    {
        public Tubematerial()
        {
            Bicycletubeusage = new HashSet<Bicycletubeusage>();
            Biketubes = new HashSet<Biketubes>();
        }

        public decimal Tubeid { get; set; }
        public string Material { get; set; }
        public string Description { get; set; }
        public decimal? Diameter { get; set; }
        public decimal? Thickness { get; set; }
        public string Roundness { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Stiffness { get; set; }
        public decimal? Listprice { get; set; }
        public string Construction { get; set; }

        public virtual ICollection<Bicycletubeusage> Bicycletubeusage { get; set; }
        public virtual ICollection<Biketubes> Biketubes { get; set; }
    }
}
