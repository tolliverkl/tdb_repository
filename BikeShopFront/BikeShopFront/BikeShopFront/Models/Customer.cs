﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Customer.cs
 * Summary: Serves as a model for a Customer
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Bicycle = new HashSet<Bicycles>();
            Customertransaction = new HashSet<Customertransaction>();
        }

        public decimal Customerid { get; set; }
        public string Phone { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Zipcode { get; set; }
        public decimal? Cityid { get; set; }
        public decimal? Balancedue { get; set; }

        public virtual City City { get; set; }
        public virtual ICollection<Bicycles> Bicycle { get; set; }
        public virtual ICollection<Customertransaction> Customertransaction { get; set; }
    }
}
