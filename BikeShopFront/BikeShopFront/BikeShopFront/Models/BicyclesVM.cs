﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BikeShopFront.Models
{
    public class BicyclesVM
    {
        public decimal Serialnumber { get; set; }
        public string ModelType { get; set; }
        public decimal? FrameSize { get; set; }
        public string Construction { get; set; }
        public decimal? ListPrice { get; set; }
    }
}
