﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Preference
 * Summary: Serves as a model for a Preference 
 */

using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Preference
    {
        public string Itemname { get; set; }
        public decimal? Value { get; set; }
        public string Description { get; set; }
        public DateTime? Datechanged { get; set; }
    }
}
