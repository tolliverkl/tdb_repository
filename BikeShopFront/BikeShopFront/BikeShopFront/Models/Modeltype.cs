﻿/*
 * Created by: Table Drop Bikes
 * Bike Shop Project: Sprint 1
 * File Name: Modeltype
 * Summary: Serves as a model for a Modeltype 
 */
using System;
using System.Collections.Generic;

namespace BikeShopFront.Models
{
    public partial class Modeltype
    {
        public Modeltype()
        {
            Bicycle = new HashSet<Bicycles>();
            Modelsize = new HashSet<Modelsize>();
        }

        public string Modeltype1 { get; set; }
        public string Description { get; set; }
        public decimal? Componentid { get; set; }

        public virtual ICollection<Bicycles> Bicycle { get; set; }
        public virtual ICollection<Modelsize> Modelsize { get; set; }
    }
}
