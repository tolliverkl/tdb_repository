﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BikeShopFront.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Newtonsoft.Json.Linq;
using NLog;
using System.Diagnostics.Eventing.Reader;
using X.PagedList;
using System.IO;
using System.Collections;

namespace BikeShopFront.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Method that retrives all the bikes
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {

            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            logger.Info("Home Page");

            List<Bicycles> bicycleList = new List<Bicycles>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("http://18.218.138.221/api/bicycles"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    bicycleList = JsonConvert.DeserializeObject<List<Bicycles>>(apiResponse);
                }
            }

            var bike = from b in bicycleList
                       select new BicyclesVM { 
                           Serialnumber = b.Serialnumber, 
                           ModelType = b.Modeltype, 
                           Construction = b.Construction, 
                           FrameSize = b.Framesize, 
                           ListPrice = b.Listprice };

            return View(bike.Take(200));
        }


        [HttpGet]
        [Route("/home/search/{min}/{max}")]
        public async Task<IActionResult> Search(decimal min, decimal max)
        {
            List<Bicycles> bicycleList = new List<Bicycles>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("http://18.218.138.221/api/bicycles"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    bicycleList = JsonConvert.DeserializeObject<List<Bicycles>>(apiResponse);
                }
            }

            var bikes = from b in bicycleList
                        select b;

            bikes = bikes.Where(x => x.Listprice >= min && x.Listprice <= max);

            return new JsonResult(bikes);
        }


        /// <summary>
        /// Method to retreive a single bike
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> GetBike(int id)
        {
            Bicycles bicycle = new Bicycles();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("http://18.218.138.221/api/bicycles/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    bicycle = JsonConvert.DeserializeObject<Bicycles>(apiResponse);
                }
            }
            return View(bicycle);
        }

        /// <summary>
        /// Get Create View Method
        /// </summary>
        public IActionResult CreateBike()
        {
            if (User.IsInRole("Admin"))
            {
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Method to Create a new Bicycle
        /// </summary>
        /// <param name="bicycles"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateBike(Bicycles bicycles)
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            if (User.IsInRole("Admin"))
            {
                Bicycles bikeToAdd = new Bicycles();
                using (var httpClient = new HttpClient())
                {
                    StringContent content = new StringContent(JsonConvert.SerializeObject(bicycles), Encoding.UTF8, "application/json");

                    using (var response = await httpClient.PostAsync("http://18.218.138.221/api/bicycles", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        bikeToAdd = JsonConvert.DeserializeObject<Bicycles>(apiResponse);

                    }
                }

                logger.Info("Admin has created a Bicycle");
                return RedirectToAction("Index");
            }
            else
            {
                logger.Warn("User attemped to create a Bicycle");
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// Get method to update a bike
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> UpdateBike(int id)
        {
            if (User.IsInRole("Admin"))
            {
                Bicycles bicycle = new Bicycles();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync("http://18.218.138.221/api/bicycles/" + id))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        bicycle = JsonConvert.DeserializeObject<Bicycles>(apiResponse);
                    }
                }
                return View(bicycle);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// Post method to update a bike
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateBike(Bicycles bicycles)
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            if (User.IsInRole("Admin"))
            {
                Bicycles bicycleToUpdate = new Bicycles();
                using (var httpClient = new HttpClient())
                {
                    var content = new MultipartFormDataContent();
                    content.Add(new StringContent(bicycles.Bicycletubeusage.ToString()), "Bicycletubeusage");
                    content.Add(new StringContent(bicycles.Bikeparts.ToString()), "Bikeparts");
                    content.Add(new StringContent(bicycles.Biketubes.ToString()), "Biketubes");
                    content.Add(new StringContent(bicycles.Chainstay.ToString()), "Chainstay");
                    content.Add(new StringContent(bicycles.Componentlist.ToString()), "Componentlist");
                    content.Add(new StringContent(bicycles.Construction), "Construction");
                    //content.Add(new StringContent(bicycles.Customer.ToString()), "Customer");
                    content.Add(new StringContent(bicycles.Customerid.ToString()), "Customerid");
                    content.Add(new StringContent(bicycles.Customname), "Customname");
                    content.Add(new StringContent(bicycles.Employeeid.ToString()), "Employeeid");
                    content.Add(new StringContent(bicycles.Frameassembler.ToString()), "Frameassembler");
                    content.Add(new StringContent(bicycles.Frameprice.ToString()), "Frameprice");
                    content.Add(new StringContent(bicycles.Framesize.ToString()), "Framesize");
                    content.Add(new StringContent(bicycles.Headtubeangle.ToString()), "Headtubeangle");
                    content.Add(new StringContent(bicycles.Letterstyleid.ToString()), "Letterstyleid");
                    content.Add(new StringContent(bicycles.Listprice.ToString()), "Listprice");
                    content.Add(new StringContent(bicycles.Modeltype), "Modeltype");
                    content.Add(new StringContent(bicycles.Orderdate.ToString()), "Orderdate");
                    content.Add(new StringContent(bicycles.Painter.ToString()), "Painter");
                    content.Add(new StringContent(bicycles.Paintid.ToString()), "Paintid");
                    content.Add(new StringContent(bicycles.Saleprice.ToString()), "Saleprice");
                    content.Add(new StringContent(bicycles.Salestate), "Salestate");
                    content.Add(new StringContent(bicycles.Salestax.ToString()), "Salestax");
                    content.Add(new StringContent(bicycles.Seattubeangle.ToString()), "Seatubeangle");
                    content.Add(new StringContent(bicycles.Serialnumber.ToString()), "Serialnumber");
                    content.Add(new StringContent(bicycles.Shipdate.ToString()), "Shipdate");
                    content.Add(new StringContent(bicycles.Shipemployee.ToString()), "Shipemployee");
                    content.Add(new StringContent(bicycles.Shipprice.ToString()), "Shipprice");
                    content.Add(new StringContent(bicycles.Startdate.ToString()), "Startdate");
                    content.Add(new StringContent(bicycles.Storeid.ToString()), "Storeid");
                    content.Add(new StringContent(bicycles.Toptube.ToString()), "Toptube");
                    content.Add(new StringContent(bicycles.Waterbottlebrazeons.ToString()), "Waterbottlebrazeons");

                    using (var response = await httpClient.PutAsync("http://18.218.138.221/api/bicycles/" + bicycles.Serialnumber, content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        bicycleToUpdate = JsonConvert.DeserializeObject<Bicycles>(apiResponse);
                    }
                }


                logger.Info("Admin has updated a Bicycle");
                return View(bicycleToUpdate);
            }
            else
            {
                logger.Warn("User attemped to update a Bicycle");
                return RedirectToAction("Index", "Home");
            }
        }

        /// <summary>
        /// </summary>
        /// Get method to Delete a Bicycle
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DeleteBike(decimal id)
        {
            if (User.IsInRole("Admin"))
            {
                Bicycles bicycle = new Bicycles();
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.GetAsync("http://18.218.138.221/api/bicycles/" + id))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        bicycle = JsonConvert.DeserializeObject<Bicycles>(apiResponse);
                    }
                }
                return View(bicycle);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Method that actually deletes the entry from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteBikeConfirmed(decimal id)
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            if (User.IsInRole("Admin"))
            {
                using (var httpClient = new HttpClient())
                {
                    using (var response = await httpClient.DeleteAsync("http://18.218.138.221/api/Bicycles/" + id))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();

                    }
                }

                logger.Info("Admin has deleted a Bicycle");
                return RedirectToAction("Index");
            }
            else
            {
                logger.Warn("User attemped to delete a Bicycle");
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult Purchase()
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            logger.Info("User has purchased a Bicycle");
            return RedirectToAction("Index");
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Covid19()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public static string GetImage()
        {
            Random random = new Random();
            string image = "";
            image = "Bike" + random.Next(1, 24) + ".jpg";
            return image;
        }


    }

}
