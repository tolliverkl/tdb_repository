﻿using BikeShopFront.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_RBS.Controllers
{
    public class RoleController : Controller
    {
        RoleManager<IdentityRole> roleManager;
        UserManager<IdentityUser> userManager;

        /// 
        /// Injecting Role Manager
        public RoleController(RoleManager<IdentityRole> roleMgr, UserManager<IdentityUser> userMrg)
        {
            roleManager = roleMgr;
            userManager = userMrg;
        }


        public IActionResult Index()
        {
            if (User.IsInRole("Admin"))
            {
                var roles = roleManager.Roles.ToList();
                return View(roles);
            }
            else
            {
                Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
                logger.Warn("User attempted to access the roles");
                return RedirectToAction("Index", "Home");
            }
        }


        public IActionResult Create()
        {
            if (User.IsInRole("Admin"))
            {
                return View(new IdentityRole());
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Create(IdentityRole role)
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();

            if (User.IsInRole("Admin"))
            {
                logger.Info("Admin created a role");
                await roleManager.CreateAsync(role);
                return RedirectToAction("Index");
            }
            else
            {
                logger.Warn("User attempted to create a role");
                return RedirectToAction("Index", "Home");
            }
        }


        public async Task<IActionResult> Update(string id)
        {
            if (User.IsInRole("Admin"))
            {
                IdentityRole role = await roleManager.FindByIdAsync(id);
                List<IdentityUser> members = new List<IdentityUser>();
                List<IdentityUser> nonMembers = new List<IdentityUser>();
                foreach (IdentityUser user in userManager.Users)
                {
                    var list = await userManager.IsInRoleAsync(user, role.Name) ? members : nonMembers;
                    list.Add(user);
                }
                return View(new RoleEdit
                {
                    Role = role,
                    Members = members,
                    NonMembers = nonMembers
                });
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> Update(RoleModification model)
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            if (User.IsInRole("Admin"))
            {
                IdentityResult result;
                if (ModelState.IsValid)
                {
                    foreach (string userId in model.AddIds ?? new string[] { })
                    {
                        IdentityUser user = await userManager.FindByIdAsync(userId);
                        if (user != null)
                        {
                            result = await userManager.AddToRoleAsync(user, model.RoleName);

                        }
                    }
                    foreach (string userId in model.DeleteIds ?? new string[] { })
                    {
                        IdentityUser user = await userManager.FindByIdAsync(userId);
                        if (user != null)
                        {
                            result = await userManager.RemoveFromRoleAsync(user, model.RoleName);

                        }
                    }
                }

                if (ModelState.IsValid)
                {
                    logger.Info("Admin updated a user to a role");
                    return RedirectToAction(nameof(Index));
                }
                else
                    return await Update(model.RoleId);
            }
            else
            {
                logger.Warn("User attempted to update a role");
                return RedirectToAction("Index", "Home");
            }

        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            Logger logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.Config").GetCurrentClassLogger();
            if (User.IsInRole("Admin"))
            {
                IdentityRole role = await roleManager.FindByIdAsync(id);
                if (role != null)
                {
                    IdentityResult result = await roleManager.DeleteAsync(role);
                    if (result.Succeeded)
                    {
                        logger.Info("Admin deleted a role");
                        return RedirectToAction("Index");
                    }
                    else
                        logger.Error("Failed to delete a role");
                }
                else
                    ModelState.AddModelError("", "No role found");
                return View("Index", roleManager.Roles);
            }
            else
            {
                logger.Warn("User attempted to delete a role");
                return RedirectToAction("Index", "Home");
            }
        }
    }
}
